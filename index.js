const axios = require('axios');
const crypto = require('crypto');
const http = require("http");


// These parameters should be used for all requests
const SUMSUB_APP_TOKEN = 'prd:lwGIElloJ8u5ewls1ELTwOk4.QwhdqNbcqxycay5QAkZxbQcAGoCQZkzz'; // Example: sbx:uY0CgwELmgUAEyl4hNWxLngb.0WSeQeiYny4WEqmAALEAiK2qTC96fBad - Please don't forget to change when switching to production
const SUMSUB_SECRET_KEY = '30pQdLL9ulGkwjIJNG45p0fjHo6vDGbB'; // Example: Hej2ch71kG2kTd1iIUDZFNsO5C1lh5Gq - Please don't forget to change when switching to production
const SUMSUB_BASE_URL = 'https://api.sumsub.com';
const SUMSUB_KYC_LEVEL='bcap-kyc-level';

const config = {};
config.baseURL= SUMSUB_BASE_URL;

axios.interceptors.request.use(createSignature, function (error) {
  return Promise.reject(error);
})

function createSignature(config) {
  console.log('Creating a signature for the request...');

  var ts = Math.floor(Date.now() / 1000);
  const signature = crypto.createHmac('sha256',  SUMSUB_SECRET_KEY);
  signature.update(ts + config.method.toUpperCase() + config.url);

  console.log(ts + config.method.toUpperCase() + config.url);
  var hexsign = signature.digest('hex');
  console.log(hexsign);
  config.headers['X-App-Access-Ts'] = ts;
  config.headers['X-App-Access-Sig'] = hexsign;


  return config;
}

function createAccessToken (userId) {
  let externalUserId = "random-JSToken-" + Math.random().toString(36).substr(2, 9);
  if(userId){
    //characters (e.g., "@", "+") it should be URL encoded otherwise you may get signature mismatch
    //userId in our case is email
    externalUserId=encodeURIComponent(userId);
  }
  const ttlInSecs=1200;

  console.log("External UserID: ", externalUserId);
  console.log("Creating an access token for initializng SDK...");

  const method = 'post';
  let url = `/resources/accessTokens?userId=${externalUserId}&ttlInSecs=${ttlInSecs}&levelName=${SUMSUB_KYC_LEVEL}`;

  const headers = {
    'Accept': 'application/json',
    'X-App-Token': SUMSUB_APP_TOKEN
  };

  config.method = method;
  config.url = url;
  config.headers = headers;
  config.data = null;

  return config;
}

const server = http.createServer(async function (req, res) {
  console.log('request is here');
  res.writeHead(200, {'Content-Type': 'application/json','Access-Control-Allow-Origin':'*'});

  const url = new URL('https://bottlecaptoken.si/resource/'+req.url);
  const userId = url.searchParams.get("userId");


  await axios(createAccessToken(userId))
      .then(function (response) {
        console.log("Response:\n", response.data);
        res.write(JSON.stringify(response.data));
        res.end();
      })
      .catch(function (error) {
        console.log("Error:\n", error);
        res.write(JSON.stringify(error));
        res.end();
      });

});

server.listen();